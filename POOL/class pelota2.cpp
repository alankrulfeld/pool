#include "class pelota.h"

pelota::pelota(bool blanca, Vector2 pos, Color color)
{
	_blanca = blanca;
	_pos = pos;
	_color = color;
	_radio = 10;
	_vel = { 0,0 };
	_res = { 0, 0 };
	_angulo = 0;
	_PDeImpc = { 0,0 };
}
void pelota::drawPelota()
{
	DrawCircle(_pos.x,_pos.y,_radio,_color);
}

void pelota::drawPelota(short i)
{
	DrawCircle(_pos.x, _pos.y, _radio, _color);
	DrawText(FormatText("%i",i),_pos.x,_pos.y,5,RED);
}



void pelota::reiniciar()
{
	if (IsKeyPressed(KEY_R))
	{
		_pos = { static_cast<float>(GetScreenWidth() / 3), static_cast<float>(GetScreenHeight() / 2) };
		_vel = { 0,0 };
	}
}

void pelota::drawPDeImpc()
{
	//lo que se hace para calcular el pinto rojo es una regla de 3 entre la distancia del mouse al centro de la pelota.
	//ya que es calculable el x e y de la distancia del mouse a la pelota.
	//y despues hago regla de 3 con el radio directo de la pelota.
	//asi obtengo la posicion en x u en y del punto rojo.
	if (_vel.x==0&&_vel.y==0)
	{
		if (!IsMouseButtonDown(MOUSE_LEFT_BUTTON))
		{
			Vector2 mouse = GetMousePosition();
			_PDeImpc.x = _pos.x - 1 * (_radio * (_pos.x - mouse.x)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));
			_PDeImpc.y = _pos.y - 1 * (_radio * (_pos.y - mouse.y)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));
			
		}
		DrawCircle(_PDeImpc.x, _PDeImpc.y, 2, RED);
	}
}

void pelota::golpearPared()
{
	if (_pos.y+_radio >= GetScreenHeight())
	{
		_vel.y *= -1;
	}
	if (_pos.y - _radio <= 0)
	{
		_vel.y *= -1;
	}
	if (_pos.x + _radio >= GetScreenWidth())
	{
		_vel.x *= -1;
	}
	if (_pos.x - _radio <= 0)
	{
		_vel.x *= -1;
	}
}

void pelota::golpeConOtraB(Vector2 pos)
{
	Vector2 puntoDColiccion;
	Vector2 distancia;

		distancia.x = pos.x - _pos.x;
		distancia.y = pos.y - _pos.y;
	//la distancia siempre es positiva, por lo tanto voy a tener que modificar el punto de coliccion.
	//para no tener que hacer lo que dice aca arriba, voy a permitir que la distancia sea negativa, para que en caso de
	//este negativo, se dibuje bien el puntoDcoliccion.
	puntoDColiccion = { distancia.x / 2, distancia.y / 2 }; //la distancia /2 deberia ser mas o menos el radio de la pelota.
	DrawCircle(puntoDColiccion.x+_pos.x,puntoDColiccion.y+_pos.y,_radio,RED);
}

void pelota::datos()
{
	if (IsKeyPressed(KEY_H))
	{
		std::cout << "Velocidad = " << _vel.x << "," << _vel.y << std::endl;
	}
}

void pelota::pegarle(Vector2 & fuerza)
{
	Vector2 mouse = GetMousePosition();
	if (_vel.x==0||_vel.y==0)
	{
		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
		{
			if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
			{
				fuerza.x = 0;
				fuerza.y = 0;
			}
			else
			{
				//para 1r cuadrante.
				int opuesto = _pos.y - mouse.y;
				int adyacente = _pos.x - mouse.x;
				float AnguloResultante = atan2(opuesto, adyacente);
				AnguloResultante = AnguloResultante * 180 / 3.14f;
				if (IsKeyPressed(KEY_A))
				{
					std::cout << "Este es el angulo:" << AnguloResultante;
				}
				_angulo = AnguloResultante;
				//esto solo se cumple cuandoestoy pegando a 45 grados.
				//fuerza.x++; // aca tiene que aumentar en algo proporcional al mouse.
				//fuerza.y++; // aca tiene que aumentar en algo proporcional al mouse.
				if (fuerza.x + fuerza.y <= 4)
				{
					fuerza.x += (_PDeImpc.x - _pos.x) / 300;
					fuerza.y += (_PDeImpc.y - _pos.y) / 300;
					std::cout << "Fuerza en X: " << fuerza.x << std::endl;
					std::cout << "-------------" << std::endl;
					std::cout << "Fuerza en Y: " << fuerza.y << std::endl;

				}
				else
				{
					//fuerza.x = 10;
					//fuerza.y = 10;
				}
				//FUERZA DE GOLPE GRAFICA.
			}


		}
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			//en un primer momento use. _PDeImpc.x>_pos.x&& _PDeImpc.y > _pos.y //pero mejor usar el angulo.

			if (_angulo >= 0 && _angulo <= 90)
			{
				//2do cuadrante. +,+;
				_vel = { -fuerza.x,-fuerza.y }; //se lo paso a la velocidad de la pelota.
			}
			if (_angulo > 90 && _angulo <= 180)
			{
				//1do cuadrante. -,+;
				_vel = { -fuerza.x,-fuerza.y }; //se lo paso a la velocidad de la pelota.
			}
			if (_angulo<0 && _angulo > -90)
			{
				//3ro cuadrante. +,-;
				_vel = { -fuerza.x,-fuerza.y }; //se lo paso a la velocidad de la pelota.
			}
			if (_angulo < -90 && _angulo > -180)
			{
				//3ro cuadrante. -,-;
				_vel = { -fuerza.x,-fuerza.y }; //se lo paso a la velocidad de la pelota.
			}
			//_vel = {fuerza.x,fuerza.y}; //se lo paso a la velocidad de la pelota.
			_res = { fabs(fuerza.x / 300),fabs(fuerza.y / 300) };
			fuerza.x = 0;
			fuerza.y = 0;

		}
	}
	
	DrawRectangle(0, GetScreenHeight() - 10, fabs(fuerza.x * 5) + fabs(fuerza.y * 5), 10, RED);
}

void pelota::actualizar()
{
	//primer cuadrante.
	_pos.x += _vel.x;
	_pos.y += _vel.y;
	
	if (_vel.x<0.09f&&_vel.x>-0.09f)
	{
		_vel.x = 0;
	}
	if (_vel.y<0.09f&&_vel.y>-0.09f)
	{
		_vel.y = 0;
	}
	
	if (_vel.x>0)
	{
		//_pos.x += _vel.x;
		_vel.x -= _res.x;
	}
	if (_vel.y>0)
	{
		//_pos.y += _vel.y;
		_vel.y -= _res.y;
	}
	if (_vel.x < 0)
	{
		//_pos.x -= _vel.x;
		_vel.x += _res.x;
	}
	if (_vel.y < 0)
	{
		//_pos.y -= _vel.y;
		_vel.y += _res.y;
	}
}

void pelota::setPosition(Vector2 pos)
{
	_pos = pos;
}

Vector2 pelota::darPos()
{
	return _pos;
}

int pelota::darRadio()
{
	return _radio;
}

Vector2 pelota::darVel()
{
	return _vel;
}

pelota::pelota(Vector2 pos, Color color)
{
	_blanca = false;
	_pos = pos;
	_color = color;
	_radio = 10;
	_vel = { 0,0 };
	_res = { 0,0 };
	_PDeImpc = { 0,0 };
	_angulo = 0;
}

taco::taco()
{
	_fuerza = { 0,0 };
}

Vector2 taco::getterFuerza()
{
	return _fuerza;
}
