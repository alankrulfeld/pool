#include "funciones.h"

bool hayColisionEntreCirculos(Vector2 centro1, float radio1, Vector2 centro2, float radio2)
{
    double cateto1 = 0.0f;
    double cateto2 = 0.0f;
    float hipotenusa = 0;

    cateto1 = fabs(static_cast<double>(centro1.x) - static_cast<double>(centro2.x));
    cateto2 = fabs(static_cast<double>(centro1.y) - static_cast<double>(centro2.y));

    hipotenusa = sqrt((cateto1 * cateto1) + (cateto2 * cateto2));

    if (hipotenusa < (radio1 + radio2))
    {
        return true;
    }
    else
    {
        return false;
    }
}