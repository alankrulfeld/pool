#include "classPelota.h"
namespace tipografia
{
	int chica = 5;
}
pelota::pelota(Vector2 pos, Color color, int n)
{
	_estado = true; //existe.
	_pos = pos;		//posicion.
	_posInit = pos; //posicion inicial.
	_color = color; //color.
	_radio = 10;	//radio.
	_vel = { 0,0 };
	_res = { 0.05f,0.05f };
	_angulo = 0;
	_PDeImpc = { 0,0 };
	for (int i = 0; i < cantidadPelotas; i++)
	{
		_col[i] = false;
	}
}
void pelota::drawPelota()
{
	DrawCircle(_pos.x,_pos.y,_radio,_color);
}

void pelota::drawPelota(short i)
{
	DrawCircle(_pos.x, _pos.y, _radio, _color);
	DrawText(FormatText("%i",i),_pos.x-MeasureText("x",tipografia::chica),_pos.y,tipografia::chica,RED);
}



void pelota::reiniciar()
{
	if (IsKeyPressed(KEY_R))
	{
		_pos = _posInit;
		_vel = { 0,0 };
		_estado = true;
	}
}

void pelota::drawPDeImpc(Vector2 x)
{
	//lo que se hace para calcular el pinto rojo es una regla de 3 entre la distancia del mouse al centro de la pelota.
	//ya que es calculable el x e y de la distancia del mouse a la pelota.
	//y despues hago regla de 3 con el radio directo de la pelota.
	//asi obtengo la posicion en x u en y del punto rojo.
	if (_vel.x==0&&_vel.y==0)
	{
		if (!IsMouseButtonDown(MOUSE_LEFT_BUTTON))
		{
			Vector2 mouse = x;
			//obtengo un punto de la circundeferencia del circulo mas proximo a el mouse.

			_PDeImpc.x = _pos.x - 1 * (_radio * (_pos.x - mouse.x)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));
			_PDeImpc.y = _pos.y - 1 * (_radio * (_pos.y - mouse.y)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));
			
		}
		DrawCircle(_PDeImpc.x, _PDeImpc.y, 2, RED);
	}
}
void pelota::drawPDeImpcB(Vector2 x)
{
	//lo que se hace para calcular el pinto rojo es una regla de 3 entre la distancia del mouse al centro de la pelota.
	//ya que es calculable el x e y de la distancia del mouse a la pelota.
	//y despues hago regla de 3 con el radio directo de la pelota.
	//asi obtengo la posicion en x u en y del punto rojo.
	Vector2 mouse = x;
	//obtengo un punto de la circundeferencia del circulo mas proximo a el mouse.

	_PDeImpc.x = _pos.x - 1 * (_radio * (_pos.x - mouse.x)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));
	_PDeImpc.y = _pos.y - 1 * (_radio * (_pos.y - mouse.y)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));

	DrawCircle(_PDeImpc.x, _PDeImpc.y, 2, RED);
			
	
}

void pelota::golpearPared()
{
	if (_pos.y+_radio >= GetScreenHeight())
	{
		_vel.y *= -1;
	}
	if (_pos.y - _radio <= 0)
	{
		_vel.y *= -1;
	}
	if (_pos.x + _radio >= GetScreenWidth())
	{
		_vel.x *= -1;
	}
	if (_pos.x - _radio <= 0)
	{
		_vel.x *= -1;
	}
}

void pelota::golpeConOtraB(Vector2 pos)
{
	Vector2 puntoDColiccion;
	Vector2 distancia;

		distancia.x = pos.x - _pos.x;
		distancia.y = pos.y - _pos.y;
	//la distancia siempre es positiva, por lo tanto voy a tener que modificar el punto de coliccion.
	//para no tener que hacer lo que dice aca arriba, voy a permitir que la distancia sea negativa, para que en caso de
	//este negativo, se dibuje bien el puntoDcoliccion.
	puntoDColiccion = { distancia.x / 2, distancia.y / 2 }; //la distancia /2 deberia ser mas o menos el radio de la pelota.
	DrawCircle(puntoDColiccion.x+_pos.x,puntoDColiccion.y+_pos.y,_radio,RED);
}

void pelota::datos()
{
	if (IsKeyPressed(KEY_H))
	{
		std::cout << "Velocidad = " << _vel.x << "," << _vel.y << std::endl;
	}
}

void pelota::pegarle(Vector2 & fuerza)
{
	Vector2 mouse = GetMousePosition();
	if (_vel.x==0&&_vel.y==0)
	{
		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
		{
			if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
			{
				//para cancelar el tirpo puedo apretar segundo boton.
				fuerza.x = 0;
				fuerza.y = 0;
			}
			else
			{
				//calculo el angulo que se forma con el centro de la pelota a el mouse.
				int opuesto = _pos.y - mouse.y;
				int adyacente = _pos.x - mouse.x;
				//atan2 = Compute arc tangent with two parameters
				/*
				double atan2 ()
				{
				
				}
				*/
				float AnguloResultante = atan2(opuesto, adyacente);
				AnguloResultante = AnguloResultante * 180 / 3.14f; //lo paso a grados celcius.
				if (IsKeyPressed(KEY_A))
				{
					std::cout << "Este es el angulo:" << AnguloResultante;
				}
				_angulo = AnguloResultante;
				/*
								90�
								 |
							  00000000
							 0000000000
						   00000000000000
					0�---- 00000000000000  ----  180� o -180� si voy desde abajo
						   00000000000000
						     0000000000
							  00000000
							     |
								-90�
				
				
				*/

				int golpe_maximo = 20;
				
				//fabs conbierte todo valor a postivo, si es -3, pasa a ser +3;
				/*
				float fabs(int n)
				{
					if (n < 0)
						n *= -1;
					return n;
				}
				*/
				if (fabs(fuerza.x) + fabs(fuerza.y) <= golpe_maximo)
				{
					//para darle medida a la fuerza, tomo la posicion de la pelota y el punto de impacto y sumos sus x e y
					//a una proporcion muy baja, por eso se divide en velocidad de carga.
					int velocidad_de_carga = 200;
					fuerza.x += (_PDeImpc.x - _pos.x) / velocidad_de_carga;
					fuerza.y += (_PDeImpc.y - _pos.y) / velocidad_de_carga;
					std::cout << "Fuerza en X: " << fuerza.x << std::endl;
					std::cout << "-------------" << std::endl;
					std::cout << "Fuerza en Y: " << fuerza.y << std::endl;

				}
				else
				{
					//se loquea la fuerza una vez pasada la distancia del golpe maximo.
				}
			}
			
		}
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{

			_vel = { -fuerza.x,-fuerza.y }; //se lo paso a la velocidad de la pelota.
			//la resistencia de la mesa en proporcional a la fuerza del golpe.
			_res = { fabs(fuerza.x / 300),fabs(fuerza.y / 300) };
			fuerza.x = 0;
			fuerza.y = 0;

		}
	}
	
	DrawRectangle(0, GetScreenHeight() - 10, fabs(fuerza.x * 5) + fabs(fuerza.y * 5), 10, RED);
}

void pelota::actualizar()
{
	//ir pasando la velocidad.
	_pos.x += _vel.x;
	_pos.y += _vel.y;
	//------------------------
	//si la velocidad es muy baja, la pelota se frena.
	if (_vel.x<0.09f&&_vel.x>-0.09f)
	{
		_vel.x = 0;
	}
	if (_vel.y<0.09f&&_vel.y>-0.09f)
	{
		_vel.y = 0;
	}
	//-------------------------
	//Se va frenando la pelota en base a la resistencia de la mesa.
	if (_vel.x>0)
	{
		_vel.x -= _res.x;
	}
	if (_vel.y>0)
	{
		_vel.y -= _res.y;
	}
	if (_vel.x < 0)
	{
		_vel.x += _res.x;
	}
	if (_vel.y < 0)
	{
		_vel.y += _res.y;
	}
	//-------------------------
}

void pelota::setPosition(Vector2 pos)
{
	_pos = pos;
}

Vector2 pelota::darPos()
{
	return _pos;
}

int pelota::darRadio()
{
	return _radio;
}

Vector2 pelota::darVel()
{
	return _vel;
}

void pelota::setVel(Vector2 vel)
{
	_vel = vel;
}

void pelota::coliciono(int n)
{
	_col[n] = true;
}

bool pelota::darCollisiono(int n)
{
	return _col[n];
}

void pelota::move()
{
	if (IsKeyDown(KEY_UP)) { _pos.y--; }
	if (IsKeyDown(KEY_DOWN)) { _pos.y++; }
	if (IsKeyDown(KEY_LEFT)) { _pos.x--; }
	if (IsKeyDown(KEY_RIGHT)) { _pos.x++; }
	
		if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON))
		{
			_pos = GetMousePosition();
		}
	
}

void pelota::stop()
{
	
		_vel = { 0,0 };

	
}

taco::taco()
{
	_fuerza = { 0,0 };
}

Vector2 taco::getterFuerza()
{
	return _fuerza;
}
