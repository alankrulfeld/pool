#ifndef FUNCIONES
#define FUNCIONES
#include "raylib.h"
#include <math.h>
bool hayColisionEntreCirculos(Vector2 centro1, float radio1, Vector2 centro2, float radio2);

#endif // !FUNCIONES

