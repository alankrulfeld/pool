#ifndef CLASS_PELOTA
#define CLASS_PELOTA
#include "raylib.h"
#include <math.h>
#include <iostream>
enum Pelotas
{
	Blanca
};
//la bola 0 va a ser la blanca.
namespace tipografia
{
	extern int chica;
}
const int cantidadPelotas = 2;

class pelota
{
public:
	//creador.
	pelota(Vector2 pos, Color color,int n);
	//dibujado.
	void drawPelota();
	void drawPelota(short i);
	//control.
	void reiniciar();
	void golpearPared();

	void drawPDeImpc(Vector2 x);
	void drawPDeImpcB(Vector2 x);
	
	void golpeConOtraB(Vector2 pos);
	void datos();
	void pegarle(Vector2 & fuerza);
	void actualizar();
	void setPosition(Vector2 pos);
	Vector2 darPos();
	int darRadio();
	Vector2 darVel();
	void setVel(Vector2 vel);
	void coliciono(int n);
	bool darCollisiono(int n);
	void move();
	void stop();

private:
	Vector2 _posInit;
	bool     _blanca;
	int	      _radio;
	Vector2		_res;
	Vector2     _pos;
	Vector2     _vel;
	Vector2	_PDeImpc;
	int		 _angulo; //en grados.
	Color     _color;
	bool		_col[cantidadPelotas]; //esta tocando con otra en este ciclo?
	int		 _Numero;
	bool	 _estado;
};

class taco
{
public:
	taco();
	Vector2 getterFuerza();
	Vector2 _fuerza;

private:
	

};



#endif // !CLASS_PELOTA
